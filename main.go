package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"log"
	"html/template"
)

// The "db" package level variable will hold the reference to our database instance
var db *sql.DB
const hashCost = 8

func hello(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Welcome to my Index!") // write data to response
}

func login(w http.ResponseWriter, r *http.Request){
	fmt.Println("method", r.Method)
	if r.Method == "GET"{
		t, _ := template.ParseFiles("login.html")
		t.Execute(w, nil)
	} else {
		r.ParseForm()
		fmt.Println("username:", r.Form["username"])
		fmt.Println("password:", r.Form["password"])
	}
}

func initDB(){
	var err error
	// Connect to the postgres db
	// you might have to change the connection string to add your database credentials
	db, err = sql.Open("postgres", "dbname=testdb sslmode=disable")
	if err != nil{
		panic(err)
	}
}

func main(){
	address := "0.0.0.0:9090"
	// "Signin" and "Signup" are handler that we will implement
	//http.HandleFunc("/register", register)
	http.HandleFunc("/", login)
	//http.HandleFunc("/login", login)
	// initialize our database connection
	//initDB()
	err:= http.ListenAndServe(address, nil)
	if err != nil{
		log.Fatal("ListenAndServer: ", err)
	}
}